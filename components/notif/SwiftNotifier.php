<?php
/**
 * Created by PhpStorm.
 * User: Vit
 * Date: 29.07.2015
 * Time: 20:55
 */

namespace app\components\notif;


use app\helpers\ValuesHelper;
use app\models\Auction;
use app\models\AuctionBid;
use app\models\AuctionProviding;
use app\models\User;
use app\modules\admin\models\Admin;
use app\modules\admin\models\AuthAssignment;
use Yii;
use yii\helpers\Url;
use app\models\DemandOffer;
use app\models\Demand;

class SwiftNotifier implements NotifierInterface
{

    /**
     * @inheritdoc
     */
    public function onRegister(User $user)
    {
        Yii::$app->mailer
            ->compose(['html' => '@app/views/auth/activateMail'], [
                'link' => Url::to(['auth/activate', 'id' => $user->id, 'hash' => $user->getActivateHash()], true)
            ])
            ->setTo($user->email)
            ->setFrom(Yii::$app->params['emailSender'])
            ->setSubject('Активация аккаунта')
            ->send();
    }

    /**
     * @inheritdoc
     */
    public function onRecoveryPassword(User $user, $newPassword)
    {
        Yii::$app->mailer
            ->compose(['html' => '@app/views/auth/recoveryMail'], [
                'password' => $newPassword
            ])
            ->setTo($user->email)
            ->setFrom(Yii::$app->params['emailSender'])
            ->setSubject('Сброс пароля')
            ->send();
    }

    private function getAdminEmailsByOperation($operations)
    {
        if (!is_array($operations)) {
            $operations = [$operations];
        }

        $ops = array();
        foreach ($operations as $operation) {
            $ops[] = $operation;
            $exploded = explode('/', $operation);
            if (isset($exploded[1])) {
                $ops[] = $exploded[0].'/*/*';
            }
            if (isset($exploded[2])) {
                $ops[] = $exploded[0].'/'.$exploded[1].'/*';
            }
        }

        $ids = AuthAssignment::find()
            ->where(['item_name' => $ops])
            ->limit(20)
            ->distinct(true)
            ->select(['user_id'])
            ->column();

        $emails = Admin::find()->where(['id' => $ids])->select('email')->column();
        if (!$emails) {
            $emails = ['admin@'];
        }

        return $emails;
    }
}
<?php

namespace app\components\notif;

use app\models\AuctionBid;
use app\models\AuctionProviding;
use app\models\Demand;
use app\models\DemandOffer;
use app\models\User;
use app\models\Auction;

interface NotifierInterface
{
    /**
     * Отправить письмо новому юзеру после регистрации
     * @param User $user
     * @return mixed
     */
    public function onRegister(User $user);

    /**
     * Отправить письмо для восстановления пароля
     * @param User $user
     * @param $newPassword
     * @return mixed
     */
    public function onRecoveryPassword(User $user, $newPassword);
}
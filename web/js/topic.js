(function(){
    $(document).ready(function() {
        $('.topic-start').on('click', function () {
            $.ajax({
                url: 'topic-start',
                dataType: 'json',
                data: {id: topicId}
            }).done(function (data) {
                $('.topic-name').html(data.topic_name);
                $('.topic-start').addClass('hide');
            });
        });

        $('.get-question').on('click', function () {
            $('.get-question').addClass('hide');
            getQuestion();
        });

        $('.skip-question').on('click', function () {
            skipQuestion();
        });

        $('.answer-question').on('click', function () {
            answerQuestion($('.answer-text').val());
        });

        $('.answer-text').on('keydown', function (e) {
            if (13 == e.keyCode) {
                e.preventDefault();
                e.stopPropagation();
                $('.answer-question').trigger('click');
            }
        });

        function skipQuestion() {
            if (timerHandle) {
                clearTimeout(timerHandle);
                timerHandle = 0;
            }
            $.ajax({
                url: 'skip-question',
                dataType: 'json'
            }).done(function (data) {
                $('.question-area').addClass('hide');
                $('.get-question').removeClass('hide');
            });
        }

        function answerQuestion(text) {
            if (timerHandle) {
                clearTimeout(timerHandle);
                timerHandle = 0;
            }
            $.ajax({
                url: 'answer-question',
                dataType: 'json',
                data: {text: text}
            }).done(function (data) {
                $('.question-area').addClass('hide');
                $('.get-question').removeClass('hide');
            });
        }

        var timerHandle = 0;

        function startCounter(s) {
            if (timerHandle) {
                clearTimeout(timerHandle);
                timerHandle = 0;
            }
            if (s < 0) {
                skipQuestion();
            } else {
                $('.question-area .question-timer').html(s);
                  timerHandle = setTimeout(function() {
                    startCounter(s - 1);
                }, 1000);
            }
        }

        function getQuestion() {
            $('.question-area').addClass('hide');
            $.ajax({
                url: 'get-question',
                dataType: 'json'
            }).done(function (data) {
                switch (data.error) {
                    case 0:
                        $('.question-area .question-text').html(data.question.text);
                        $('.question-area .question-value span').html(data.question.value);
                        $('.question-area .answer-text').val('');
                        $('.question-area').removeClass('hide');
                        startCounter(data.time);
                        break;
                    case 1:
                        window.location.replace('/');
                        break;
                    case 2:
                        window.location.replace('auth/login');
                        break;
                    case 3:
                        window.location.replace('/');
                        break;
                }

            });
        }
    });
})();
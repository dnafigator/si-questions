<?php

use yii\db\Migration;

class m160218_204453_add_show_date_to_topic extends Migration
{
    public function up()
    {
        $this->addColumn('topic', 'show_date', 'date NULL default NULL after name');
    }

    public function down()
    {
        $this->dropColumn('topic', 'show_date');
    }
}

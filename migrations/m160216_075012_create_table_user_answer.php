<?php

use yii\db\Migration;

class m160216_075012_create_table_user_answer extends Migration
{
    public function up()
    {
        $this->createTable('user_answer', [
            'id' => $this->primaryKey(),
            'user_id' => 'int(11)',
            'question_id' => 'int(11)',
            'text' => 'text',
            'skipped' => 'tinyint(1) NULL default NULL',
            'is_correct' => 'tinyint(1) NULL default NULL',
            'create_date' => 'datetime NOT NULL',
            'answer_date' => 'datetime NULL default NULL',
            'modify_date' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
        ], 'ENGINE=InnoDB CHARSET=utf8');

        $this->addForeignKey('user_answer_id', 'user_answer', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('question_answer_id', 'user_answer', 'question_id', 'question', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('user_answer');
    }
}

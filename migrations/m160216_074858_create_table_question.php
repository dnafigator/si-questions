<?php

use yii\db\Migration;

class m160216_074858_create_table_question extends Migration
{
    public function up()
    {
        $this->createTable('question', [
            'id' => $this->primaryKey(),
            'topic_id' => 'int(11)',
            'text' => 'text not null',
            'value' => 'int(11) not null',
            'create_date' => 'datetime NOT NULL',
            'modify_date' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
        ], 'ENGINE=InnoDB CHARSET=utf8');
        $this->addForeignKey('question_topic_id', 'question', 'topic_id', 'topic', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('question');
    }
}

<?php

use yii\db\Migration;

class m160218_204347_add_name_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'name', 'varchar(250) not null after email');
    }

    public function down()
    {
        $this->dropColumn('user', 'name');
    }
}

<?php

use yii\db\Migration;

class m160216_042232_create_table_user extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'email' => 'varchar(100) NOT NULL',
            'hpassw' => 'varchar(60) NOT NULL',
            'auth_key' => 'varchar(32) not null',
            'is_enabled' => 'tinyint(1) not null default 0',
            'create_date' => 'datetime NOT NULL',
            'modify_date' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
        ], 'ENGINE=InnoDB CHARSET=utf8');
    }

    public function down()
    {
        $this->dropTable('user');
    }
}

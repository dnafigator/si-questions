<?php

use yii\db\Migration;

class m160220_180927_add_birthdate_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'birthdate', 'date NOT NULL after name');
    }

    public function down()
    {
        $this->dropColumn('user', 'birthdate');
    }
}

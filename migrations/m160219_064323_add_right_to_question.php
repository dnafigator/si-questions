<?php

use yii\db\Migration;

class m160219_064323_add_right_to_question extends Migration
{
    public function up()
    {
        $this->addColumn('question', 'right', 'text NULL default NULL after text');
    }

    public function down()
    {
        $this->dropColumn('question', 'right');
    }
}

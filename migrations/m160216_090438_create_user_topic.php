<?php

use yii\db\Migration;

class m160216_090438_create_user_topic extends Migration
{
    public function up()
    {
        $this->createTable('user_topic', [
            'id' => $this->primaryKey(),
            'user_id' => 'int(11)',
            'topic_id' => 'int(11)',
            'end_date' => 'datetime NULL default NULL',
            'create_date' => 'datetime NOT NULL',
            'modify_date' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
        ], 'ENGINE=InnoDB CHARSET=utf8');

        $this->addForeignKey('user_topic_user_id', 'user_topic', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('user_topic_topic_id', 'user_topic', 'topic_id', 'topic', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('user_topic');
    }
}

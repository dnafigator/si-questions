<?php

use yii\db\Migration;

class m160216_074759_create_table_topic extends Migration
{
    public function up()
    {
        $this->createTable('topic', [
            'id' => $this->primaryKey(),
            'name' => 'varchar(250) not null',
            'create_date' => 'datetime NOT NULL',
            'modify_date' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
        ], 'ENGINE=InnoDB CHARSET=utf8');
    }

    public function down()
    {
        $this->dropTable('topic');
    }
}

<?php
namespace app\commands;

use yii\console\Controller;
use app\models\Topic;
use app\models\Question;
use Yii;

class InitController extends Controller
{
    public function actionClearTopics()
    {
        //Yii::$app->db->createCommand('TRUNCATE user_answer; TRUNCATE user_topic; TRUNCATE topic; TRUNCATE question;')->execute();
    }
    public function actionTopics()
    {
        $datadir = 'data';
        $d = opendir ($datadir);
        while ($f = readdir($d)) {
            $fname = $datadir.'/'.$f;
            if (is_file($fname)) {
                $qus = file ($fname);
                echo "\n".'Loading topic from '.$fname;
                if (!isset($qus[0])) {
                    echo '...fail'."\n";
                    continue;
                }

                $tr = Yii::$app->db->beginTransaction();
                $topic = new Topic();
                $topic->show_date = trim($qus[0]);
                $topic->name = trim($qus[1]);
                $topic->save();
                echo "\n".' name "'.$topic->name.'"';
                unset ($qus[0]);
                unset ($qus[1]);
                foreach ($qus as $question) {
                    $qu = new Question();
                    $qu->topic_id = $topic->id;
                    $question = trim($question);
                    $m = [];
                    if ( preg_match("/([0-9]+) (.+)\/(.+)\//", $question, $m) ) {
                        $qu->right = trim($m[3]);
                    } else {
                        preg_match("/([0-9]+) (.+)/", $question, $m);
                    }
                    $qu->value = (int)$m[1];
                    $qu->text = trim($m[2]);
                    echo "\n\t".' question value = '.$qu->value.' text="'.$qu->text.'"';
                    if (!$qu->save())
                    {
                        echo '...error! rolling back topic'."\n";
                        $tr->rollBack();
                        $tr = 0;
                        break;
                    }
                }

                if ($tr) {
                    $tr->commit();
                    echo "\n".'success'."\n";
                }
            }
        }

    }
}

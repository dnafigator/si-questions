<?php

namespace app\controllers;

use app\models\forms\RegisterForm;
use yii\helpers\Url;
use app\models\User;
use yii\web\HttpException;
use Yii;

class AuthController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['site/index']));
        }

        if (!Yii::$app->request->isPost) {
            return $this->render('loginForm');
        } else {
            $emailOrLogin = Yii::$app->request->post('email_or_login');
            $password = Yii::$app->request->post('password');

            if (mb_strpos($emailOrLogin, '@', null, 'utf-8') !== false) {
                $user = User::findOne(['email' => $emailOrLogin]);
            } else {
                $user = User::findOne(['login' => $emailOrLogin]);
            }

            if (!$user || !$user->validatePassword($password)) {
                $error = 'Неправильные имя и/или пароль - попробуйте, пожалуйста, снова.';
            } elseif (!$user->is_enabled) {
                $error = 'Пользователь не активирован. Вход невозможен.';
            } else {
                $error = false;
            }

            if ($error) {
                return $this->render('loginForm', [
                    'error' => $error
                ]);
            }

            $duration = Yii::$app->request->post('remember', false) ? 3600 * 24 * 7 : 0;
            Yii::$app->user->login($user, $duration);

            return $this->redirect(Url::to(['site/index']));
        }
    }
    public function actionLogout()
    {
        if (!\Yii::$app->user->isGuest) {
            \Yii::$app->user->logout();
        }
        return $this->redirect(Url::to(['site/index']));
    }

    public function actionRegister()
    {
        $modelRegister = new RegisterForm();
        if (Yii::$app->request->post()) {
            $modelRegister->setData(Yii::$app->request->post());

            $modelRegister->user->is_enabled = 0;

            if ($modelRegister->validate()) {
                $modelRegister->toRegister();
                $this->getNotifier()->onRegister($modelRegister->user);
                return $this->redirect('/auth/register-success');
            } else {
                return $this->render('registerForm', [
                    'modelRegister' => $modelRegister
                ]);
            }
        } else {
            return $this->render('registerForm', [
                'modelRegister' => $modelRegister
            ]);
        }
    }
    public function actionRegisterSuccess()
    {
        return $this->render('registerSuccess');
    }
    public function actionActivate($id, $hash)
    {
        /** @var User $user */
        $user = User::findOne($id);
        if (!$user || $hash != $user->getActivateHash()) {
            throw new HttpException(404);
        }
        $user->is_enabled = 1;
        $user->save();
        Yii::$app->user->login($user);
        return $this->render('activateSuccess');
    }


    public function actionRecovery()
    {
        if (Yii::$app->request->isPost) {
            $email = Yii::$app->request->post('email');
            $user = User::findOne(['email' => $email]);
            if (!$user) {
                return $this->render('recoveryForm', [
                    'error' => 'Пользователь не найден'
                ]);
            }

            $newPassword = Yii::$app->security->generateRandomString(12);
            $user->setNewPassword($newPassword);
            if (!$user->save()) {
                throw new HttpException(500, 'Ошибка записи в БД');
            }

            $this->getNotifier()->onRecoveryPassword($user, $newPassword);

            return $this->render('recoveryForm', [
                'success' => true
            ]);
        } else {
            return $this->render('recoveryForm');
        }
    }
}

<?php

namespace app\controllers;

use app\models\Question;
use Yii;
use app\models\UserAnswer;
use app\models\UserAnswerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\HttpException;

/**
 * SearchController implements the CRUD actions for UserAnswer model.
 */
class SearchController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserAnswer models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest || !in_array ( Yii::$app->user->getIdentity()->email, Yii::$app->params['adminEmails'])) {
            throw new HttpException(401);
        }
        $searchModel = new UserAnswerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination->pageSize=1000;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCorrect($id)
    {
        if(!in_array ( Yii::$app->user->getIdentity()->email, Yii::$app->params['adminEmails'])) {
            throw new HttpException(401);
        }
        $ua = UserAnswer::findOne($id);
        $ua->is_correct = 1;
        $ua->save();
        $this->redirect(Yii::$app->request->referrer);
    }

    public function actionWrong($id)
    {
        if(!in_array ( Yii::$app->user->getIdentity()->email, Yii::$app->params['adminEmails'])) {
            throw new HttpException(401);
        }
        $ua = UserAnswer::findOne($id);
        $ua->is_correct = 0;
        $ua->save();

        $this->redirect(Yii::$app->request->referrer);
    }

    private static function printTristate($val, $vals)
    {
        if ( null === $val ) return $vals[0];
        else return $val ? $vals[2] : $vals[1];
    }
    private static function answerTime($d1, $d2) {
        $datetime1 = date_create($d1);
        $datetime2 = date_create($d2);
        $interval = date_diff($datetime1, $datetime2);
        return $interval->format("%s");
    }

    public function actionExport()
    {
        if(!in_array ( Yii::$app->user->getIdentity()->email, Yii::$app->params['adminEmails'])) {
            throw new HttpException(401);
        }
        $ua = UserAnswer::find()->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=si-'.date('Y-m-d_H-i-s').'.csv');
        header("Pragma: no-cache");
        $outF = fopen('php://temp', 'r+');
        $header = null;
        foreach($ua as $answer) {
            $data = [
                'id' => $answer->id,
                'user_id' => $answer->user->id,
                'e-mail' => $answer->user->email,
                'ФИО' => $answer->user->name,
                'Дата рождения' => $answer->userBirtdate,
                'topic_id' => $answer->question->topic->id,
                'Тема' => $answer->question->topic->name,
                'Дата темы' => $answer->question->topic->show_date,
                'q_id' => $answer->question->id,
                'Цена вопроса' => $answer->question->value,
                'Текст вопроса' => $answer->question->text,
                'Ответ' => $answer->text,
                'Правильный ответ' => $answer->question->right,
                'Пропущен' => $this->printTristate($answer->skipped, ['-', 'Нет', 'Да']),
                'Правильный' => $this->printTristate($answer->is_correct, ['-', 'Нет', 'Да']),
                'Время ответа' => $this->answerTime($answer->create_date, $answer->answer_date),
            ];
            if (!$header) {
                foreach ( $data as $h => $v ) {
                    $header[] = $h;
                }
                fputcsv($outF, $header, ';', '"');
            }
            fputcsv($outF, $data, ';', '"');
        }
        rewind($outF);
        $res = '';
        while($str = fgets($outF)) {
            $res .= $str;
        }
        fclose ($outF);
        return $res;
    }

    /**
     * Finds the UserAnswer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserAnswer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
/*    protected function findModel($id)
    {
        if (($model = UserAnswer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
*/
}

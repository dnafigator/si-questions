<?php
/**
 * Created by PhpStorm.
 * User: Vit
 * Date: 29.07.2015
 * Time: 21:08
 */

namespace app\controllers;


use app\components\notif\NotifierInterface;
use yii\web\Controller;

class BaseController extends Controller
{
    /**
     * @return NotifierInterface
     */
    public function getNotifier()
    {
        return \Yii::$app->notifier;
    }
} 
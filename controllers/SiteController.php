<?php

namespace app\controllers;

use app\models\Topic;
use app\models\UserTopic;
use app\models\Question;
use app\models\UserAnswer;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
/*
    public function beforeAction($action)
    {
        return 0;
    }
*/
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest)
        {
            return $this->redirect(Url::to('/site/about'));
        }
        $topics = Topic::find()->orderBy(['show_date' => 'ASC'])->all();
        return $this->render('index', [
            'topics' => $topics
        ]);
    }

    public function actionTopic()
    {
        $id = yii::$app->request->get('id');

        $active = UserTopic::getActive();
        $topic = $id ? Topic::isTodayTopic($id) : ($active ? $active->getTopic()->one() : 0);
        if ( !$topic )
        {
            return $this->redirect('/');
        }
        return $this->render('topic', ['topic' => $topic]);
    }

    public function actionTopicStart($id)
    {
        if (!UserTopic::getActive()) {
            $today = Topic::isTodayTopic($id);
            if ( !$today ) {
                return $this->redirect('/');
            }
            $ut = new UserTopic();
            $ut->user_id = Yii::$app->user->getIdentity()->id;
            $ut->topic_id = $id;
            $ut->save();
        }
        return $this->redirect(Url::to(['topic']));
    }

    public function actionAnswerQuestion($text)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) return ['error' => 2, 'text' => 'Необходимо залогиниться'];

        $ua = UserAnswer::getActive();
        if(!$ua) return ['error' => 3, 'text' => 'Нет активного вопроса'];
        $ua->text = $text;
        $ua->skipped = 0;
        if (
            $ua->text &&
            $ua->question->right &&
            mb_strtolower($ua->text) == mb_strtolower($ua->question->right)) {
            $ua->is_correct = 1;
        }
        $ua->answer_date = date('Y-m-d H:i:s');
        $ua->save();
        return ['error' => 0];
    }

    public function actionSkipQuestion()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->user->isGuest) return ['error' => 2, 'text' => 'Необходимо залогиниться'];
        $user = Yii::$app->user->getIdentity();
        $ua = UserAnswer::getActive();
        if(!$ua) return ['error' => 3, 'text' => 'Нет активного вопроса'];
        $ua->skipped = 1;
        $ua->is_correct = 0;
        $ua->answer_date = date('Y-m-d H:i:s');
        $ua->save();
        return ['error' => 0];
    }

    private $answerTime = 20;
    public function actionGetQuestion()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) return ['error' => 2, 'text' => 'Необходимо залогиниться'];
        $user = Yii::$app->user->getIdentity();

        $active = UserTopic::getActive();
        if(!$active) return ['error' => 3, 'text' => 'Нет активной темы'];

        $ua = UserAnswer::getActive();
        if ($ua) {
            $datetime1 = date_create($ua->create_date);
            $datetime2 = date_create(date('Y-m-d H:i:s'));
            $interval = date_diff($datetime1, $datetime2);
            $time = $this->answerTime - $interval->format("%s");
            return ['error' => 0, 'time' => $time, 'question' => $ua->getQuestion()->one()];
        }

        $question = Question::find()
            ->where('question.id NOT IN (SELECT user_answer.question_id FROM user_answer WHERE user_answer.user_id='.$user->id.')')
            ->andWhere(['question.topic_id' => $active->topic_id])
            ->orderBy(['question.id' => 'ASC'])
            ->one();
//die($question->sql);
        if ( !$question ) {
            $active->end_date = date('Y-m-d H:i:s');
            $active->save();
            return ['error' => 1, 'text' => 'Тема закончилась'];
        }

        $userAnswer = new UserAnswer();
        $userAnswer->user_id = $user->id;
        $userAnswer->question_id = $question->id;
        $userAnswer->save();
        return ['error' => 0, 'time' => $this->answerTime, 'question' => $question];
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionDateTest()
    {
        echo date('Y-m-d H:i:s').'</br>';
        echo \app\models\Topic::startTime.'</br>';
        echo (date('H:i:s') < \app\models\Topic::startTime) ? 'LATER</br>' : 'NOW</br>';
        $SQLtime = yii::$app->db->createCommand('SELECT NOW() as t')->queryAll();
        echo $SQLtime[0]['t'].'</br>';
    }
}

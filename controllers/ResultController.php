<?php

namespace app\controllers;

use Yii;
use app\models\UserTopic;
use app\models\UserTopicSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ResultController implements the CRUD actions for UserTopic model.
 */
class ResultController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserTopic models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest || !in_array ( Yii::$app->user->getIdentity()->email, Yii::$app->params['adminEmails'])) {
            throw new HttpException(401);
        }
        $searchModel = new UserTopicSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination->pageSize=255;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionExport()
    {
        if(!in_array ( Yii::$app->user->getIdentity()->email, Yii::$app->params['adminEmails'])) {
            throw new HttpException(401);
        }
        $ut = UserTopic::find()->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=si-'.date('Y-m-d_H-i-s').'.csv');
        header("Pragma: no-cache");
        $outF = fopen('php://temp', 'r+');
        $header = null;
        foreach($ut as $topic) {
            $data = [
                'id' => $topic->id,
                'user_id' => $topic->user->id,
                'e-mail' => $topic->userEmail,
                'ФИО' => $topic->userName,
                'Дата рождения' => $topic->userBirtdate,
                'topic_id' => $topic->topic_id,
                'Тема' => $topic->topicName,
                'Дата темы' => $topic->topic->show_date,
                'Результат' => $topic->topicResult,
                'Начато' => $topic->create_date,
                'Закончено' => $topic->end_date,
            ];
            if (!$header) {
                foreach ( $data as $h => $v ) {
                    $header[] = $h;
                }
                fputcsv($outF, $header, ';', '"');
            }
            fputcsv($outF, $data, ';', '"');
        }
        rewind($outF);
        $res = '';
        while($str = fgets($outF)) {
            $res .= $str;
        }
        fclose ($outF);
        return $res;
    }

    /**
     * Finds the UserTopic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserTopic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserTopic::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

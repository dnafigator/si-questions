<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserAnswerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ответы';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::a("Экспорт", "/search/export", ['target' => '_blank'])?>
<div class="user-answer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--p>
        <?= Html::a('Create User Answer', ['create'], ['class' => 'btn btn-success']) ?>
    </p-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'userEmail',
            'userName',
            //'topicName',
            //'questionText',
            [
                'attribute' =>'questionValue',
                'headerOptions' => ['width' => '60px'],
            ],
            'questionRight',
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'correct' => function($url,$model){
                        if($model->skipped || 1 === $model->is_correct) return '';
                        return Html::a("<span class='glyphicon glyphicon-plus-sign' style='color:green;'></span>", $url);
                    },
                ],
                'template' => '{correct}',
            ],
            'text',
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'wrong' => function($url,$model){
                        if($model->skipped || 0 === $model->is_correct) return '';
                        return Html::a("<span class='glyphicon glyphicon-minus-sign' style='color:red;'></span>", $url);
                    },
                ],
                'template' => '{wrong}',
            ],
            //'skipped',
            [
                'attribute' =>'is_correct',
                'headerOptions' => ['width' => '10px'],
            ],
            [
                'attribute' => '?',
                'label' => 'Время ответа',
                'format' => 'integer',
                'content' => function($m) {
                    $datetime1 = date_create($m->create_date);
                    $datetime2 = date_create($m->answer_date);
                    $interval = date_diff($datetime1, $datetime2);
                    $val = $interval->format("%s");
                    if($val > 20) {
                        return '<span style="color:red;font-weight: bold;">'.$val.'</span>';
                    } else {
                        return '<span style="color:green;font-weight: bold;">'.$val.'</span>';
                    }
                }
            ],
            'create_date',
            //'modify_date',
        ],
    ]); ?>

</div>

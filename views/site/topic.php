<?php

use \yii\helpers\Html;
use yii\helpers\Url;
use app\models\enums\EnumTopicStatus;

$this->title = 'Тема';

$status = $topic->getStatus();
$status_id = $status['status'];
$this->registerJsFile('@web/js/topic.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<div class="site-topic">
    <div class="topic-name"><?php echo $topic->name;?><span><?php echo EnumTopicStatus::getName($status_id)?></span></div>
    <?php switch($status_id) {
        case EnumTopicStatus::IS_NEW:
            echo '<div class="topic-start-line">'.
                Html::a("Приступить", Url::to(['topic-start', 'id' => $topic->id]), ['class' => "topic-start"]).
                '</div>';
            break;
        case EnumTopicStatus::IN_PROGRESS:?>
            <div class="topic-start-line">
                <?=Html::a("Показать вопрос", '#', ['class' => "get-question"]);?>
            </div>
            <div class="question-area hide">
                <div class="question-value">Вопрос за <span></span></div>
                <div class="question-text-wrap">
                    <div class="question-text"></div><br/>
                    <div class="answer-line">
                        <?php echo Html::textInput("answer-text", '', ['class' => 'answer-text']);?>
                        <?php echo Html::button("Ответить", ['class' => 'answer-question']);?><br/>
                        <?php echo Html::button("Пропустить", ['class' => 'skip-question']);?>
                    </div>
                    <div class="question-timer"></div>
                </div>
            </div>
            <?php break;
    }?>
</div>
<script lang="js">
    var topicId = <?php echo $topic->id;?>;
</script>
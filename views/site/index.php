<?php

/* @var $this yii\web\View */
use \yii\helpers\Html;
use yii\helpers\Url;
use \app\models\enums\EnumTopicStatus;

$this->title = 'СИ';
?>
<div class="site-index">
   <!--Темы отыгрываются по одной в сутки с 12:00 до 24:00 MSK-->
<?
foreach ($topics as $topic) {
   $status = $topic->getStatus()['status'];
   $showName = '???';
   switch ($status) {
      case EnumTopicStatus::NOT_YET:
      case EnumTopicStatus::TODAY_BUT_LATER:
         continue;
      case EnumTopicStatus::UNAVAILABLE:
      case EnumTopicStatus::COMPLETED:
         $showName = $topic->name;
         break;
      case EnumTopicStatus::IS_NEW:
      case EnumTopicStatus::IN_PROGRESS:
         $showName = Html::a($topic->name, Url::to(['topic', 'id' => $topic->id]));
         break;
   }
   echo '<div class="topic-line">' . $showName . ' ' . '<span>' . EnumTopicStatus::getName($status) . '</span></div>';
}?>
</div>

<div class="ta-container">

    <h1>Сброс пароля</h1>

    <? if (!isset($success)): ?>
        <p>Введите адрес электронный почты, указанный при регистрации.</p>
        <form action="<?=\yii\helpers\Url::to(['auth/recovery'])?>" method="post" class="ta-recovery-form">
            <label for="emailForRecovery">E-mail</label>
            <input type="email" placeholder="E-mail" class="form-control" id="emailForRecovery" name="email">
            <input type="hidden" value="<?=\Yii::$app->request->csrfToken?>" name="_csrf">
            <? if (isset($error)): ?>
                <div class="ta-form-error"><?=$error?></div>
            <? endif; ?>
            <button type="submit" class="ta-button">Сбросить пароль</button>
        </form>
    <? else: ?>
        <p>Новый пароль отправлен на указанный Вами адрес электронной почты.</p>
    <? endif ?>
</div>
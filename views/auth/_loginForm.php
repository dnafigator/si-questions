<form class="ta-login-form ta-form" method="post" action="<?=\yii\helpers\Url::to(['auth/login'])?>">
    <label for="inputEmailOrLogin" class="sr-only">E-mail</label>
    <input name="email_or_login" type="text" id="inputEmailOrLogin" class="form-control" placeholder="E-mail" required="" autofocus="">
    <label for="inputPassword" class="sr-only">Пароль</label>
    <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Пароль" required="">
    <div class="checkbox">
        <label>
            <input name="remember" type="checkbox" value="1"> Запомнить меня
</label>
    </div>
    <input type="hidden" name="_csrf" value="<?=\Yii::$app->request->csrfToken?>"/>
    <button class="ta-button" type="submit">Войти</button>
    <a class="ta-login-recovery" href="<?=\yii\helpers\Url::to(['auth/recovery'])?>">Забыли пароль?</a>
</form>
<p class="ta-form-error"><?=!empty($error) ? $error : ''?></p>
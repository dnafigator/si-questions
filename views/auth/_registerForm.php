<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use app\models\enums\EnumCompanyType;

?>

<?php

if (!isset($modelRegister)) {
    $modelRegister = new \app\models\forms\RegisterForm();
}

$form = ActiveForm::begin([
    'layout' => 'horizontal',
    'enableClientValidation' => false,
    'action' => Url::to(['auth/register']),
    'options' => [
        'method' => 'POST',
        'class' => 'ta-register-form ta-form'
    ]
]);
?>

<div class="ta-register-user">
    <?php
    echo $form->field($modelRegister->user, 'email')->textInput();
    echo $form->field($modelRegister->user, 'name')->textInput();
    echo $form->field($modelRegister->user, 'birthdate')->widget(\yii\jui\DatePicker::className(), [
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
        'clientOptions' => [
            'changeMonth' => true,
            'changeYear'  => true,
            'yearRange'   => (date('Y')-100).':'.(date('Y')),
            //'minDate'     => 0,
        ],
    ]);
        //->textInput();
    echo $form->field($modelRegister->user, 'password')->passwordInput();
    echo $form->field($modelRegister->user, 'password_repeat')->passwordInput();

    ?>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-3">
        <button class="ta-button ta-register-submit" type="submit">Зарегистрироваться</button>
    </div>
</div>


<? ActiveForm::end(); ?>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserTopicSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Результаты';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=Html::a("Экспорт", "/result/export", ['target' => '_blank'])?>
<div class="user-topic-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'user_id',
            'userEmail',
            'userName',
            //'topic_id',
            'topicName',
            'topicResult',
            'create_date',
            'end_date',
            // 'modify_date',
        ],
    ]); ?>

</div>

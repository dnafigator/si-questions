<?php

return [
    'adminEmail' => 'admin@example.com',
    'emailSender' => [
        EMAIL_SENDER => 'Своя Игра'
    ],
    'adminEmails' => [
        'dan@bronyakin.ru',
        'venya1985@gmail.com',
        'eugenelyapin@gmail.com',
        'malkin@chgk.info',
		'elyapin_88@mail.ru',
    ],
];

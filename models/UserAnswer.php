<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_answer".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $question_id
 * @property string $text
 * @property integer $skipped
 * @property integer $is_correct
 *
 * @property Question $question
 * @property User $user
 */
class UserAnswer extends \app\models\base\MyActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'question_id', 'skipped', 'is_correct'], 'integer'],
            [['text'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'question_id' => 'Question ID',
            'text' => 'Ответ',
            'skipped' => 'Пропущен',
            'is_correct' => 'Правильный',
            'userEmail' => 'e-mail',
            'userName' => 'ФИО',
            'questionText' => 'Текст вопроса',
            'questionValue' => 'Баллы',
            'questionRight' => 'Правильный ответ',
            'topicName' => 'Тема',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserEmail()
    {
        return $this->user->email;
    }

    public function getUserName()
    {
        return $this->user->name;
    }

    public function getUserBirtdate()
    {
        return $this->user->birthdate;
    }

    public function getQuestionText()
    {
        return $this->question->text;
    }

    public function getQuestionValue()
    {
        return $this->question->value;
    }

    public function getQuestionRight()
    {
        return $this->question->right;
    }

    public function getTopicName()
    {
        return $this->question->topic->name;
    }
/*
    public function getTopic()
    {
        return $this->hasOne(Topic::className(), ['id' => 'user_id']);
        //return $this->question->topic;
    }
*/
    public static function getActive()
    {
        $user = Yii::$app->user->getIdentity();
        $ua = UserAnswer::find()->where(['user_id' => $user->id, 'text' => null, 'skipped' => null])->one();
        return $ua;
    }
}

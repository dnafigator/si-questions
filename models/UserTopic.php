<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_topic".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $topic_id
 * @property string $end_date
 * @property string $create_date
 * @property string $modify_date
 *
 * @property Topic $topic
 * @property User $user
 */
class UserTopic extends \app\models\base\MyActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'topic_id'], 'integer'],
            [['end_date', 'create_date', 'modify_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'topic_id' => 'Topic ID',
            'end_date' => 'End Date',
            'topicName' => 'Название темы',
            'userEmail' => 'E-mail',
            'userName' => 'ФИО',
            'topicResult' => 'Результат',
            'create_date' => 'Начал',
            'end_date' => 'Закончил',
            'modify_date' => 'Modify Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(Topic::className(), ['id' => 'topic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getActive()
    {
        $user = Yii::$app->user->getIdentity();
        $ut = UserTopic::find()->where(['user_id' => $user->id, 'end_date' => null])->one();
        return $ut;
    }

    public function getUserEmail()
    {
        return $this->user->email;
    }

    public function getUserName()
    {
        return $this->user->name;
    }

    public function getUserBirtdate()
    {
        return $this->user->birthdate;
    }

    public function getTopicName()
    {
        return $this->topic->name;
    }
    public function getTopicResult()
    {
        $answers = UserAnswer::find()
            ->joinWith('question')
            ->where([
                'user_answer.user_id' => $this->user->id,
                'user_answer.is_correct' => 1,
                'question.topic_id' => $this->topic->id,
            ])->sum('question.value');
        return $answers;
    }}

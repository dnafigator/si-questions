<?php

namespace app\models\forms;


use app\models\User;
use yii\base\Exception;
use yii\base\Model;


class RegisterForm extends Model
{
    public $user;

    public function __construct($config=[])
    {
        parent::__construct($config);
        $this->user = new User();
    }

    public function validate()
    {
        return $this->user->validate();
    }

    public function setData($data)
    {
        $this->user->setAttributes($data['User']);
    }

    public function toRegister()
    {
        $tr = $this->user->getDb()->beginTransaction();
        if (!$this->user->save(false)) {
            $tr->rollBack();
            throw new Exception('Ошибка сохранения данных пользователя. Попробуйте позже или обратитесь в службу поддержки.');
        }
        $tr->commit();
    }
} 
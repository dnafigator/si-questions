<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserTopic;

/**
 * UserTopicSearch represents the model behind the search form about `app\models\UserTopic`.
 */
class UserTopicSearch extends UserTopic
{
    public $userEmail;
    public $userName;
    public $topicName;
    public $topicResult;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'topic_id'], 'integer'],
            [['end_date', 'create_date', 'modify_date', 'topicName', 'userEmail', 'userName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserTopic::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes' => [
                'userEmail' => [
                    'asc' => ['user.email' => SORT_ASC],
                    'desc' => ['user.email' => SORT_DESC],
                    'label' => 'e-mail',
                    'default' => SORT_ASC
                ],
                'userName' => [
                    'asc' => ['user.name' => SORT_ASC],
                    'desc' => ['user.name' => SORT_DESC],
                    'label' => 'ФИО',
                    'default' => SORT_ASC
                ],
                'topicName' => [
                    'asc' => ['topic.name' => SORT_ASC],
                    'desc' => ['topic.name' => SORT_DESC],
                    'label' => 'ФИО',
                    'default' => SORT_ASC
                ],
            ]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith('user')->joinWith('topic');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'topic_id' => $this->topic_id,
            'end_date' => $this->end_date,
            'create_date' => $this->create_date,
            'modify_date' => $this->modify_date,
        ]);

        $query->joinWith([
            'user' => function($q) {
                $q
                    ->where('user.email LIKE "%' . $this->userEmail . '%"')
                    ->andWhere ('user.name LIKE "%' . $this->userName . '%"');
            },
            'topic' => function($q) {
                $q->where('topic.name LIKE "%' . $this->topicName . '%"');
            },
        ]);

        //$query->andFilterWhere(['like', 'user_answer.text', $this->text]);

        return $dataProvider;
    }
}

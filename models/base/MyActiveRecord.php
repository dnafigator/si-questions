<?php
/**
 * Created by PhpStorm.
 * User: vit
 * Date: 16.01.15
 * Time: 20:37
 */

namespace app\models\base;


use yii\db\ActiveRecord;

class MyActiveRecord extends ActiveRecord
{
    public function beforeSave($insert)
    {
        if ($insert && $this->isNewRecord && $this->hasAttribute('create_date')) {
            $this->create_date = date('Y-m-d H:i:s');
        }
        return parent::beforeSave($insert);
    }
} 
<?php

namespace app\models\enums;


class EnumTopicStatus
{
    const UNKNOWN = -1;
    const IS_NEW = 0;
    const IN_PROGRESS = 1;
    const COMPLETED = 2;
    const UNAVAILABLE = 3;
    const NOT_YET = 4;
    const TODAY_BUT_LATER = 5;

    public static function getAll()
    {
        return [
            ['id' => self::IS_NEW, 'name' => self::getName(self::IS_NEW)],
            ['id' => self::IN_PROGRESS, 'name' => self::getName(self::IN_PROGRESS)],
            ['id' => self::COMPLETED, 'name' => self::getName(self::COMPLETED)],
            ['id' => self::UNAVAILABLE, 'name' => self::getName(self::UNAVAILABLE)],
            ['id' => self::NOT_YET, 'name' => self::getName(self::NOT_YET)],
            ['id' => self::TODAY_BUT_LATER, 'name' => self::getName(self::TODAY_BUT_LATER)],
        ];
    }

    public static function getName($id)
    {
        switch ($id) {
            case self::IS_NEW:
                return 'Не приступал';
            case self::IN_PROGRESS:
                return 'В процессе';
            case self::COMPLETED:
                return 'Завершена';
            case self::UNAVAILABLE:
                return 'Недоступна';
            case self::NOT_YET:
                return 'Не сегодня!';
            case self::TODAY_BUT_LATER:
                return 'Сегодня c 12:00';
        }
        return 'Неизвестно';
    }
} 
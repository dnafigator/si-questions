<?php

namespace app\models;

use app\models\enums\EnumTopicStatus;
use Yii;

/**
 * This is the model class for table "topic".
 *
 * @property integer $id
 * @property string $name
 * @property string $show_date
 *
 * @property Question[] $questions
 */
class Topic extends \app\models\base\MyActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'show_date' => 'Дата отыгрыша',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['topic_id' => 'id']);
    }

    const startTime = '12:00:00';

    public static function isTodayTopic($id) {
        if (date('H:i:s') < Topic::startTime ) {
            return null;
        }
        //return Topic::find()->where (['id' => $id, 'show_date' => date('Y-m-d')])->one();
        return Topic::findOne($id);
    }

    public function getStatus()
    {
/*        if ( date('Y-m-d') > $this->show_date) {
            return ['status' => EnumTopicStatus::UNAVAILABLE];
        } else if ( date('Y-m-d') < $this->show_date) {
            return ['status' => EnumTopicStatus::NOT_YET];
        } else if (date('H:i:s') < Topic::startTime) {
            return ['status' => EnumTopicStatus::TODAY_BUT_LATER];
        }
*/
        $user = Yii::$app->user->getIdentity();
        $usertopic = UserTopic::find()->where(['user_id' => $user->id, 'topic_id' => $this->id])->one();
        if ( !$usertopic ) {
            return ['status' => EnumTopicStatus::IS_NEW];
        } else if ( null == $usertopic->end_date ) {
            return ['status' => EnumTopicStatus::IN_PROGRESS];
        } else {
            return ['status' => EnumTopicStatus::COMPLETED, 'date_completed' => $usertopic->end_date];
        }
    }
}

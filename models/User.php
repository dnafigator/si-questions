<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $email
 * @property string $name
 * @property string $birthdate
 * @property string $hpassw
 * @property string $auth_key
 * @property integer $is_enabled
 * @property string $create_date
 * @property string $modify_date
 */
class User extends \app\models\base\MyActiveRecord implements \yii\web\IdentityInterface
{
    public $password;
    public $password_repeat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name', 'birthdate'], 'required'],
            [['is_enabled'], 'integer'],
            //[['birthdate'], 'date'],
            [['create_date', 'modify_date'], 'safe'],
            [['email', 'name'], 'string', 'max' => 100],
            [['hpassw'], 'string', 'max' => 60],
            [['email'], 'unique'],
            [['password', 'password_repeat'], 'required', 'when' => function($model){return $model->isNewRecord;}],
            [['password'], 'string', 'min' => 6],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'E-mail',
            'name' => 'ФИО',
            'birthdate' => 'Дата рождения (ГГГГ-ММ-ДД)',
            'hpassw' => 'Hpassw',
            'is_enabled' => 'Is Enabled',
            'create_date' => 'Create Date',
            'modify_date' => 'Modify Date',
            'password'    => 'Пароль',
            'password_repeat'    => 'Повторите пароль',
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert && $this->isNewRecord) {
            $this->auth_key = Yii::$app->getSecurity()->generateRandomString(32);
        }
        if ($this->password !== null && $this->password !== '') {
            $this->setNewPassword($this->password);
        }
        return parent::beforeSave($insert);
    }

    /**
     * Set new password hash by password
     * @param string $password
     */
    public function setNewPassword($password)
    {
        $this->hpassw = Yii::$app->getSecurity()->generatePasswordHash($password);
    }
    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        $user = self::findOne($id);
        if ($user && !$user->is_enabled) {
            return null;
        }
        return $user;
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @throws \yii\base\NotSupportedException
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new \yii\base\NotSupportedException('Authentication by access token is not support');
    }
    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $authKey === $this->getAuthKey();
    }

    /**
     * @param string $password
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->hpassw);
    }

    /**
     * @return string
     */
    public function getActivateHash()
    {
        return sha1($this->id.$this->auth_key).md5($this->id.$this->auth_key);
    }
}

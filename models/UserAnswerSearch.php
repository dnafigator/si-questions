<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserAnswer;

/**
 * UserAnswerSearch represents the model behind the search form about `app\models\UserAnswer`.
 */
class UserAnswerSearch extends UserAnswer
{
    public $userEmail;
    public $userName;
    public $questionText;
    public $topicName;
    public $questionValue;
    public $questionRight;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questionValue', 'id', 'user_id', 'question_id', 'skipped', 'is_correct'], 'integer'],
            [['questionRight', 'questionValue', 'topicName', 'userEmail', 'userName', 'questionText', 'text', 'create_date', 'modify_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserAnswer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'userEmail' => [
                    'asc' => ['user.email' => SORT_ASC],
                    'desc' => ['user.email' => SORT_DESC],
                    'label' => 'e-mail',
                    'default' => SORT_ASC
                ],
                'userName' => [
                    'asc' => ['user.name' => SORT_ASC],
                    'desc' => ['user.name' => SORT_DESC],
                    'label' => 'ФИО',
                    'default' => SORT_ASC
                ],
            ]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith('user')->joinWith('question');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'question_id' => $this->question_id,
            'skipped' => $this->skipped,
            'is_correct' => $this->is_correct,
            'create_date' => $this->create_date,
            'modify_date' => $this->modify_date,
        ]);

        $query->joinWith([
            'user' => function($query) {
                if ($this->userEmail) {
                    $query->andWhere(['like', 'user.email', $this->userEmail]);
                }
                if ($this->userName) {
                    $query->andWhere(['like', 'user.name', $this->userName]);
                }
            },
            'question' => function($query) {
                if ($this->questionText) {
                    $query->andWhere(['like', 'question.text', $this->questionText]);
                }
                if ($this->questionRight) {
                    $query->andWhere(['like', 'question.right', $this->questionRight]);
                }
                if ($this->questionValue) {
                    $query->andWhere(['question.value' => $this->questionValue]);
                }
            },
            /*'question.topic' => function($q) {
                $q->where('topic.name LIKE "%' . $this->topicName . '%"');
            },*/
        ]);

        $query->andFilterWhere(['like', 'user_answer.text', $this->text]);

        return $dataProvider;
    }
}
